<?php
$x = 10;
echo"$x Increment is: ";
echo ++$x; //Increments $x by one, then returns $x
echo"</br>";
$x = 115;
echo"$x Decrement is: ";
echo --$x;
$x = 10;
echo"</br>";
echo "Post-increment: ";
echo $x++; //Returns $x, then increments $x by one

echo"</br> Post-decrement: ";
$x = 10;
echo $x--; //Returns $x, then decrements $x by one
?>
